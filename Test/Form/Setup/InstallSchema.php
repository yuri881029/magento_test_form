<?php
namespace Test\Form\Setup;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        //START: install stuff
        //START table setup
        $table = $installer->getConnection()->newTable(
            $installer->getTable('test_form_usermessage')
        )->addColumn(
            'test_form_usermessage_id',
            Table::TYPE_INTEGER,
            null,
            [ 'identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true, ],
            'Entity ID'
        )->addColumn(
            'user_name',
            Table::TYPE_TEXT,
            255,
            [ 'nullable' => false, ],
            'User Name'
        )->addColumn(
            'user_email',
            Table::TYPE_TEXT,
            255,
            [ 'nullable' => false, ],
            'User Email'
        )->addColumn(
                'message',
                Table::TYPE_TEXT,
                '2M',
                [ 'nullable' => false, ],
                'User Message'
        )->addColumn(
            'creation_time',
            Table::TYPE_TIMESTAMP,
            null,
            [ 'nullable' => false, 'default' => Table::TIMESTAMP_INIT, ],
            'Creation Time'
        )->addColumn(
            'it_was_answered',
            Table::TYPE_BOOLEAN,
            null,
            [ 'nullable' => false, 'default' => '0', ],
            'Answer message status'
        )->addColumn(
        'answer_time',
        Table::TYPE_TIMESTAMP,
        null,
        [ 'nullable' => true],
        'Answer message Time'
    );
        $installer->getConnection()->createTable($table);
        //END:   install stuff
        $installer->endSetup();
    }
}
