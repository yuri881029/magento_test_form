<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 28/05/16
 * Time: 13:19
 */
use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE,
                             'Test_Form',
                             __DIR__
);