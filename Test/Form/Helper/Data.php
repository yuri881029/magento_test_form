<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 29/05/16
 * Time: 20:33
 */

namespace Test\Form\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_ENABLED='test/form/enabled';

    /**
     * Check if module is enabled
     * @return mixed
     */

    public function isEnabled()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
        //return true;
    }
    

}