<?php
namespace Test\Form\Model\ResourceModel\UserMessage;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Test\Form\Model\UserMessage','Test\Form\Model\ResourceModel\UserMessage');
    }
}
