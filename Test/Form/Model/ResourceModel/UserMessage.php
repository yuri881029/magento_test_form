<?php
namespace Test\Form\Model\ResourceModel;
class UserMessage extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('test_form_usermessage','test_form_usermessage_id');
    }
}
