<?php
namespace Test\Form\Model;
class UserMessage extends \Magento\Framework\Model\AbstractModel implements UserMessageInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'test_form_usermessage';
    const YES= 1;
    const NO= 0;

    protected function _construct()
    {
        $this->_init('Test\Form\Model\ResourceModel\UserMessage');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
