<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 28/05/16
 * Time: 16:46
 */

namespace Test\Form\Controller;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Test\Form\Helper\Data;
use Test\Form\Model\UserMessageFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;


abstract class Index extends Action
{
    protected $scopeConfig;
    protected $storeManager;
    protected $helper;
    protected $resultPageFactory;
    protected $resultFowardFactory;
    protected $trasportBuilder;
    protected $userMessageFactory;
    protected $dateTime;

    /**
     * Recipient email config path
     */
    const XML_PATH_EMAIL_RECIPIENT = 'email_setting/identities/store_contact_email';

    /**
     * Sender email config path
     */
    const XML_PATH_EMAIL_SENDER = 'email_setting/identities/account_sender';

    /**
     * Email template config path
     */
    const XML_PATH_EMAIL_TEMPLATE = 'email_setting/email_conf/template';
    
    public function __construct(Context $context,
                                PageFactory $resultPageFactory,
                                ForwardFactory $resultForwardFactory,
                                StoreManagerInterface $storeManager,
                                ScopeConfigInterface $scopeConfig,
                                Data $helper,
                                TransportBuilder $transportBuilder,
                                UserMessageFactory $userMessageFactory,
                                DateTime $dateTime)
    {
        $this->resultPageFactory=$resultPageFactory;
        $this->resultFowardFactory=$resultForwardFactory;
        $this->storeManager=$storeManager;
        $this->scopeConfig=$scopeConfig;
        $this->helper=$helper;
        $this->trasportBuilder = $transportBuilder;
        $this->userMessageFactory= $userMessageFactory;
        $this->dateTime=$dateTime;

        parent::__construct($context);
    }

    /**
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     * @throws NotFoundException
     */
    public function dispatch(RequestInterface $request)
    {
         if (!$this->helper->isEnabled()) {
            throw new NotFoundException(__('Page not found.'));
        }

        return parent::dispatch($request);
    }


}