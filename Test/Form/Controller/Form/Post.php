<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 29/05/16
 * Time: 22:50
 */

namespace Test\Form\Controller\Form;




use Magento\Framework\App\Area;
use Magento\Framework\DataObject;
use Magento\Store\Model\Store;
use Test\Form\Controller\Index;
use Magento\Store\Model\ScopeInterface;

class Post extends Index
{

   
    /**
     * @throws \Zend_Validate_Exception
     */
    public function execute()
    {
        $post=$this->getRequest()->getPostValue();
        $error=false;
        $name=$post['name'];
        $email=$post['email'];
        $message=$post['message'];
        if($post)
        {
            if (!\Zend_Validate::is(trim($name), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($message), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($email), 'EmailAddress')) {
                $error = true;
            }

        }

        if (!$error){
            $storeScope = ScopeInterface::SCOPE_STORE;
            $dataObj= new DataObject();
            $dataObj->setData($post);

            try {
                //TODO save data[in process]
                $save=$this->userMessageFactory->create();
                $save->setUserName($name);
                $save->setUserEmail($email);
                $save->setMessage($message);
                $save->save();

                //TODO send mail[done]- configure the local mail service with PHP
                /*send mail to store contact
                -if the email configuration in the server is not seted the costumer will recive error message*/
                $transport = $this->trasportBuilder
                    ->setTemplateIdentifier($this->scopeConfig->getValue(self::XML_PATH_EMAIL_TEMPLATE,$storeScope))
                    ->setTemplateOptions([
                        'area' => Area::AREA_FRONTEND,
                        'store' => Store::DEFAULT_STORE_ID
                    ])
                    ->setTemplateVars(['data'=>$dataObj])
                    ->setFrom($this->scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER,$storeScope))
                    ->addTo($this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT,$storeScope))
                    ->setReplyTo(//replayTo use de data of the form
                        $email,
                        $name
                    )
                    ->getTransport();
                $transport->sendMessage();
                $this->messageManager->addSuccessMessage(__('Success'));
                $this->_redirect('postsuccess/messages/success');
                return;
            }catch (\Exception $e){
                $this->messageManager->addErrorMessage(__('Sorry!! We can not proccess your request now, try in other moment'));
                $this->_redirect('postunsuccess/messages/error');
            }
        }
        else{
            $this->messageManager->addErrorMessage(__('Fill the form with correct data'));
            $this->_redirect('testcontactus');
            return;
        }

    }
}