<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 3/06/16
 * Time: 1:00
 */

namespace Test\Form\Controller\Messages;


use Test\Form\Controller\Index;

class Success extends Index
{


    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $result=$this->resultPageFactory->create();
        $result->getConfig()->getTitle()->set('Thanks for Contact Us');
        return $result;
    }
}