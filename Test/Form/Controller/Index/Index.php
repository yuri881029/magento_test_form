<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 28/05/16
 * Time: 16:45
 */

namespace Test\Form\Controller\Index;

use Test\Form\Controller\Index as MyIndex;

class Index extends MyIndex
{
     /**
     * @param PageFactory $resultPageFactory
     * @param ForwardFactory $resultForwardFactory
     */
    

    public function execute()
    {
        $resultPage=$this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set('Contact Us');
        if (!$resultPage)
        {
            $fowardPage=$this->resultFowardFactory->create();
            return $fowardPage->forward('noroute');
        }
        return $resultPage;
    }
}