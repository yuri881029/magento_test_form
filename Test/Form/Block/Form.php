<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 29/05/16
 * Time: 22:16
 */

namespace Test\Form\Block;


use Magento\Framework\View\Element\Template;
use Test\Form\Helper\Data;

class Form extends Template
{
    /**
     * Form constructor.
     * @param Template\Context $context
     * @param array $data
     */
    protected $helper;

    public function __construct(Template\Context $context,array $data=[])
    {
        parent::__construct($context,$data);
        $this->_isScopePrivate = true;
    }


     /**
     * @return string
     */

    public function getFormAction()
    {
        return $this->getUrl('testcheckmessage/form/post',['_secure'=>true]);
    }




    /**
     * @return mixed
     */

    

}