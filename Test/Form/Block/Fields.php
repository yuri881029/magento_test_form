<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 9/06/16
 * Time: 19:49
 */

namespace Test\Form\Block;


class Fields extends \Zend_Form
{
    public function init()
    {
        // Set the method for the display form to POST
        $this->setMethod ('post');

        // Add an email element
        $this->addElement ('text', 'email', array(
            'label' => 'Your email address:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                'EmailAddress',
            )
        ));

        // Add the comment element
        $this->addElement ('textarea', 'comment', array(
            'label' => 'Please Comment:',
            'required' => true,
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 20))
            )
        ));
    }
}