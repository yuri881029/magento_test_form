<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 3/06/16
 * Time: 16:55
 */

namespace Test\Form\Block\Messages;


use Magento\Framework\View\Element\Template;

class Success extends Template
{
    public function __construct(Template\Context $context, array $data=[])
    {
        parent::__construct($context,$data);
        $this->_isScopePrivate = true;
    }

    public function getMessage()
    {
        return 'We already get your request, we will in touch soon, Best Regards';
    }
    
    

}